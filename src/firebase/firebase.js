import { initializeApp } from "firebase/app";
import { getFirestore, collection } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBraz7HkvYDlOM6vngH_NngaoVXXhISAJs",
  authDomain: "filmyverse-ee6e7.firebaseapp.com",
  projectId: "filmyverse-ee6e7",
  storageBucket: "filmyverse-ee6e7.appspot.com",
  messagingSenderId: "683339637066",
  appId: "1:683339637066:web:649aa85eebb68d7789e1b3",
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const movieRef = collection(db, "movies");
export const reviewsRef = collection(db, "reviews");
export const usersRef = collection(db, "users");

export default app;
