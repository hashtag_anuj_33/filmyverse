import { getDocs } from "firebase/firestore";
import React, { useEffect, useState } from "react";
import { Audio, ThreeDots } from "react-loader-spinner";
import ReactStar from "react-stars";
import { movieRef } from "../firebase/firebase";
import { Link } from "react-router-dom";

const Cards = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function getData() {
      setLoading(true);
      const _data = await getDocs(movieRef);
      console.log(_data);
      _data.forEach((doc) => {
        setData((prv) => [...prv, { ...doc.data(), id: doc.id }]);
      });
      setLoading(false);
    }
    getData();
  }, []);
  return (
    <div className="text-white  px-2  flex flex-wrap justify-between">
      {loading ? (
        <div className="w-full flex justify-center items-center h-96">
          {" "}
          <ThreeDots height={40} color="white" />
        </div>
      ) : (
        data.map((e, i) => {
          return (
            <Link to={`/detail/${e.id}`}>
              <div
                key={i}
                className="card mt-4 p-2 hover:-translate-y-2 cursor-pointer font-bold transition-all duration-500"
              >
                <img className="h-60 md:h-72" src={e.image} />
                <h1> {e.title}</h1>
                <h1 className="flex items-center">
                  <span className="text-gray-500">Reating :</span>
                  <ReactStar
                    size={20}
                    half={true}
                    value={e.rating / e.rated}
                    edit={false}
                  />
                </h1>
                <h1>
                  <span>Year:</span>
                  {e.year}
                </h1>
              </div>
            </Link>
          );
        })
      )}
    </div>
  );
};

export default Cards;
