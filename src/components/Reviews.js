import React, { useContext, useEffect, useState } from "react";
import ReactStar from "react-stars";
import { reviewsRef, db } from "../firebase/firebase";
import {
  addDoc,
  doc,
  updateDoc,
  query,
  where,
  getDocs,
} from "firebase/firestore";
import { TailSpin, ThreeDots } from "react-loader-spinner";
import swal from "sweetalert";
import { Appstate } from "../App";
import { useNavigate } from "react-router-dom";
const Reviews = ({ id, prevRating, userRated }) => {
  const useAppstate = useContext(Appstate);
  const navigate = useNavigate();
  const [rating, setRating] = useState(0);
  const [loading, setLoading] = useState(false);
  const [reviewsLoading, setReviewsLoading] = useState(false);
  const [form, setForm] = useState("");
  const [data, setData] = useState([]);
  const [newAdded, setNewAdded] = useState(0);

  const sendReview = async () => {
    setLoading(true);
    try {
      if (useAppstate.login) {
        await addDoc(reviewsRef, {
          movieid: id,
          name: "useAppState.userName",
          rating: rating,
          thought: form,
          timestamp: new Date().getTime(),
        });
        const ref = doc(db, "movies", id);
        await updateDoc(ref, {
          rating: prevRating + rating,
          rated: userRated + 1,
        });
        setRating(0);
        setForm("");
        setNewAdded(newAdded + 1);
        swal({
          title: "review sent",
          icon: "success",
          buttons: false,
          timer: 3000,
        });
      } else {
        navigate("/login");
      }
    } catch (error) {
      swal({
        title: error.message,
        icon: "error",
        buttons: false,
        timer: 3000,
      });
    }
    setLoading(false);
  };

  useEffect(() => {
    async function getData() {
      setReviewsLoading(true);

      let quer = query(reviewsRef, where("movieid", "==", id));
      const querySnapshot = await getDocs(quer);
      querySnapshot.forEach((doc) => {
        setData((prev) => [...prev, doc.data()]);
      });
      setReviewsLoading(false);
    }
    getData();
  }, [newAdded]);
  return (
    <div className="mt-5 border-t-2 border-gray-700 w-full">
      <ReactStar
        size={35}
        half={true}
        value={rating}
        onChange={(rate) => setRating(rate)}
      />
      <input
        value={form}
        onChange={(e) => setForm(e.target.value)}
        placeholder="Share your Reviews..."
        className="w-full p-3 outline-none header"
      />
      <button
        onClick={sendReview}
        className="w-full p-2 bg-green-500 flex justify-center"
      >
        {loading ? <TailSpin height={25} color="white" /> : "Share"}
      </button>

      {reviewsLoading ? (
        <div className="mt-7 flex justify-center">
          <ThreeDots height={12} color="white" />{" "}
        </div>
      ) : (
        <div className="mt-5 ">
          {data.map((e, i) => {
            return (
              <div
                className="header border-b border-gray p-3 w-full mt-3"
                key={i}
              >
                <div className="flex items-center">
                  <p className="text-cyan-500">{e.name}</p>
                  <p className="ml-4 text-xs">
                    ({new Date(e.timestamp).toLocaleString()})
                  </p>
                </div>
                <ReactStar
                  size={20}
                  half={true}
                  value={e.rating}
                  edit={false}
                />
                <p>{e.thought}</p>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Reviews;
